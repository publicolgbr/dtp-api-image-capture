class EnvironmentConfigurationProvider {
  constructor() {
    this.bucket = process.env.BUCKET;
    this.accessKeyId = process.env.DMSAWS_ACCESS_KEY_ID;
    this.secretAccessKey = process.env.DMSAWS_SECRET_ACCESS_KEY;
    this.kafkaBroker = process.env.KAFKA_BROKERS || 'dtp-integration-kafka.dtp.appgate.com:9092';
    this.topicFirtsCapture = process.env.TOPIC_FIRST_CAPTURE || 'dtp-web-first-capture-output';
    this.topicCapture = process.env.TOPIC_CAPTURE || 'dtp-web-capture-output';
    this.geoScreenShotApiKey = process.env.GEO_APIKEY;
    this.geoScreenShotEndPoint = process.env.GEO_ENDPOINT || 'https://apipro.geoscreenshot.com';
    this.userAgent = process.env.USER_AGENT || 'x/x'
  }
}

module.exports = EnvironmentConfigurationProvider;
