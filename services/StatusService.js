const superagent = require('superagent');
require('superagent-proxy')(superagent);

class StatusService {
  configureProxy(proxy) {
    this.proxy = proxy;
  }

  async takeStatus(url, agent) {
    console.log(`\u{1F310} select proxy: ${this.proxy.url} ${this.proxy.port} status`);

    let response = {};
    try {
      response = await superagent.head(url).set('User-Agent', agent).proxy(`http://${this.proxy.url}:${this.proxy.port}`).disableTLSCerts();
    } catch (error) {
      response.status = error.status;
    }
    return response.status;
  }
}

module.exports = StatusService;