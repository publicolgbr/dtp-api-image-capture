const puppeteer = require('puppeteer');

class BrowserService {
  configureProxy(proxy) {
    this.proxy = proxy;
  }

  async takeScreenShot(url) {
    console.log(`\u{1F310} select proxy: ${this.proxy.url} ${this.proxy.port} image`);
    const browser = await puppeteer.launch({
      ignoreHTTPSErrors: true,
      args: [`--proxy-server=${this.proxy.url}:${this.proxy.port}`, '--disable-gpu', '--disable-software-rasterizer'],
    });
    const page = await browser.newPage();
    let dataOut = {};
    try {
      page.setViewport({ width: 1920, height: 1080 });      
      let response = await page.goto(url, {
        waitUntil: 'networkidle0',
      });      
      dataOut.image = await page.screenshot({ type: 'png' });
      dataOut.html = await page.content();
      dataOut.status = response.status();
    } catch (error) {
      console.error(error);
      dataOut.image = '';
      dataOut.html = '';
      dataOut.status = 10500;
    } finally {
      await page.close();
      await browser.close();
    }
    return dataOut;
  }
}

module.exports = BrowserService;
