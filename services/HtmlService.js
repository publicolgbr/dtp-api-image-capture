const superagent = require('superagent');
require('superagent-proxy')(superagent);

class HtmlService {
  configureProxy(proxy) {
    this.proxy = proxy;
  }

  async takeHtml(url, agent) {
    console.log(`\u{1F310} select proxy: ${this.proxy.url} ${this.proxy.port} html`);

    let response = {};
    try {
      response = await superagent.get(url).set('User-Agent', agent).proxy(`http://${this.proxy.url}:${this.proxy.port}`).disableTLSCerts();
    } catch (error) {
      response.text = '';
      response.status = error.status;
    }
    return {
      data: response.text,
      status: response.status,
    };
  }
}

module.exports = HtmlService;
