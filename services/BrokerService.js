const { Kafka } = require('kafkajs');

class BrokerService {
  constructor(options) {
    this.configurationProvider = options.configurationProvider;
    this.kafka = new Kafka({
      clientId: 'web-capture-app',
      brokers: this.configurationProvider.kafkaBroker.split(','),
      connectionTimeout: 3000,
    });
  }

  async connect() {
    this.producer = this.kafka.producer();
    await this.producer.connect();
  }

  async push(message) {
    let nameTopic = message.metadata.firstCapture ? this.configurationProvider.topicFirtsCapture : this.configurationProvider.topicCapture;
    console.log(`\u{1F4AC} sending broker: ${this.configurationProvider.kafkaBroker.split(',')} topic: ${nameTopic}`);
    await this.producer.send({
      topic: nameTopic,
      messages: [{ value: JSON.stringify(message) }],
    });
  }
}

module.exports = BrokerService;
