const aws = require('aws-sdk');

class BucketService {
  constructor(options) {
    this.configurationProvider = options.configurationProvider;
    this.s3 = new aws.S3({
      accessKeyId: this.configurationProvider.accessKeyId,
      secretAccessKey: this.configurationProvider.secretAccessKey,
    });
  }

  uploadBucket(data, fileName) {
    return new Promise((resolve, reject) => {
      const params = {
        Bucket: this.configurationProvider.bucket,
        Key: fileName,
        Body: data,
      };

      let location = null;
      this.s3.upload(params, function (err, data) {
        if (err) {
          return reject(err);
        }
        location = data.Location;        
        resolve(location);
      });
    });
  }
}

module.exports = BucketService;
