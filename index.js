const EnvironmentConfigurationProvider = require('./configuration/EnvironmentConfigurationProvider');
const ExpressController = require('./controller/ExpressController');
const BucketService = require('./services/BucketService');
const BrokerService = require('./services/BrokerService');

const environmentConfigurationProvider = new EnvironmentConfigurationProvider();

const bucketService = new BucketService({
  configurationProvider: environmentConfigurationProvider,
});

const brokerService = new BrokerService({
  configurationProvider: environmentConfigurationProvider,
});

const expressController = new ExpressController({
  environmentConfiguration: environmentConfigurationProvider,
  bucketService,
  brokerService,
});

brokerService
  .connect()
  .then(() => {
    expressController.start();
  })
  .catch((error) => {
    console.log(error);
  });


