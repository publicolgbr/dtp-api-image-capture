const BrowserService = require('../services/BrowserService');
const StatusService = require('../services/StatusService');
const HtmlService = require('../services/HtmlService');

const uuid = require('uuid');

function getProxy(country) {
  let entrypoints = {
    us: { url: 'us-pr.oxylabs.io', port: 10000 },
    ca: { url: 'ca-pr.oxylabs.io', port: 30000 },
    gb: { url: 'gb-pr.oxylabs.io', port: 20000 },
    de: { url: 'de-pr.oxylabs.io', port: 30000 },
    fr: { url: 'fr-pr.oxylabs.io', port: 40000 },
    es: { url: 'es-pr.oxylabs.io', port: 10000 },
    it: { url: 'it-pr.oxylabs.io', port: 20000 },
    se: { url: 'se-pr.oxylabs.io', port: 30000 },
    gr: { url: 'gr-pr.oxylabs.io', port: 40000 },
    pt: { url: 'pt-pr.oxylabs.io', port: 10000 },
    nl: { url: 'nl-pr.oxylabs.io', port: 20000 },
    be: { url: 'be-pr.oxylabs.io', port: 30000 },
    ru: { url: 'ru-pr.oxylabs.io', port: 40000 },
    ua: { url: 'ua-pr.oxylabs.io', port: 10000 },
    pl: { url: 'pl-pr.oxylabs.io', port: 20000 },
    il: { url: 'il-pr.oxylabs.io', port: 20000 },
    tr: { url: 'tr-pr.oxylabs.io', port: 30000 },
    au: { url: 'au-pr.oxylabs.io', port: 40000 },
    my: { url: 'my-pr.oxylabs.io', port: 10000 },
    th: { url: 'th-pr.oxylabs.io', port: 20000 },
    kr: { url: 'kr-pr.oxylabs.io', port: 30000 },
    jp: { url: 'jp-pr.oxylabs.io', port: 40000 },
    ph: { url: 'ph-pr.oxylabs.io', port: 10000 },
    sg: { url: 'sg-pr.oxylabs.io', port: 20000 },
    cn: { url: 'cn-pr.oxylabs.io', port: 30000 },
    hk: { url: 'hk-pr.oxylabs.io', port: 40000 },
    tw: { url: 'tw-pr.oxylabs.io', port: 10000 },
    in: { url: 'in-pr.oxylabs.io', port: 20000 },
    pk: { url: 'pk-pr.oxylabs.io', port: 30000 },
    ir: { url: 'ir-pr.oxylabs.io', port: 40000 },
    id: { url: 'id-pr.oxylabs.io', port: 10000 },
    az: { url: 'az-pr.oxylabs.io', port: 20000 },
    kz: { url: 'kz-pr.oxylabs.io', port: 30000 },
    ae: { url: 'ae-pr.oxylabs.io', port: 40000 },
    mx: { url: 'mx-pr.oxylabs.io', port: 10000 },
    br: { url: 'br-pr.oxylabs.io', port: 20000 },
    ar: { url: 'ar-pr.oxylabs.io', port: 30000 },
    cl: { url: 'cl-pr.oxylabs.io', port: 40000 },
    pe: { url: 'pe-pr.oxylabs.io', port: 10000 },
    ec: { url: 'ec-pr.oxylabs.io', port: 20000 },
    co: { url: 'co-pr.oxylabs.io', port: 30000 },
    za: { url: 'za-pr.oxylabs.io', port: 40000 },
    eg: { url: 'eg-pr.oxylabs.io', port: 10000 },
    sa: { url: 'sa-pr.oxylabs.io', port: 44000 },
    dk: { url: 'dk-pr.oxylabs.io', port: 19000 },
    ao: { url: 'ao-pr.oxylabs.io', port: 40000 },
    cm: { url: 'cm-pr.oxylabs.io', port: 41000 },
    cf: { url: 'cf-pr.oxylabs.io', port: 42000 },
    td: { url: 'td-pr.oxylabs.io', port: 43000 },
    bj: { url: 'bj-pr.oxylabs.io', port: 44000 },
    et: { url: 'et-pr.oxylabs.io', port: 45000 },
    dj: { url: 'dj-pr.oxylabs.io', port: 46000 },
    gm: { url: 'gm-pr.oxylabs.io', port: 47000 },
    gh: { url: 'gh-pr.oxylabs.io', port: 48000 },
    ci: { url: 'ci-pr.oxylabs.io', port: 49000 },
    ke: { url: 'ke-pr.oxylabs.io', port: 10000 },
    lr: { url: 'lr-pr.oxylabs.io', port: 11000 },
    mg: { url: 'mg-pr.oxylabs.io', port: 12000 },
    ml: { url: 'ml-pr.oxylabs.io', port: 13000 },
    mr: { url: 'mr-pr.oxylabs.io', port: 14000 },
    mu: { url: 'mu-pr.oxylabs.io', port: 15000 },
    ma: { url: 'ma-pr.oxylabs.io', port: 16000 },
    mz: { url: 'mz-pr.oxylabs.io', port: 17000 },
    ng: { url: 'ng-pr.oxylabs.io', port: 18000 },
    sn: { url: 'sn-pr.oxylabs.io', port: 19000 },
    sc: { url: 'sc-pr.oxylabs.io', port: 20000 },
    zw: { url: 'zw-pr.oxylabs.io', port: 21000 },
    ss: { url: 'ss-pr.oxylabs.io', port: 22000 },
    sd: { url: 'sd-pr.oxylabs.io', port: 23000 },
    tg: { url: 'tg-pr.oxylabs.io', port: 24000 },
    tn: { url: 'tn-pr.oxylabs.io', port: 25000 },
    ug: { url: 'ug-pr.oxylabs.io', port: 26000 },
    zm: { url: 'zm-pr.oxylabs.io', port: 27000 },
    af: { url: 'af-pr.oxylabs.io', port: 28000 },
    bh: { url: 'bh-pr.oxylabs.io', port: 29000 },
    bd: { url: 'bd-pr.oxylabs.io', port: 30000 },
    am: { url: 'am-pr.oxylabs.io', port: 31000 },
    bt: { url: 'bt-pr.oxylabs.io', port: 32000 },
    mm: { url: 'mm-pr.oxylabs.io', port: 33000 },
    kh: { url: 'kh-pr.oxylabs.io', port: 34000 },
    ge: { url: 'ge-pr.oxylabs.io', port: 36000 },
    iq: { url: 'iq-pr.oxylabs.io', port: 37000 },
    jo: { url: 'jo-pr.oxylabs.io', port: 38000 },
    lb: { url: 'lb-pr.oxylabs.io', port: 39000 },
    mv: { url: 'mv-pr.oxylabs.io', port: 40000 },
    mn: { url: 'mn-pr.oxylabs.io', port: 41000 },
    om: { url: 'om-pr.oxylabs.io', port: 42000 },
    qa: { url: 'qa-pr.oxylabs.io', port: 43000 },
    sa: { url: 'sa-pr.oxylabs.io', port: 44000 },
    vn: { url: 'vn-pr.oxylabs.io', port: 45000 },
    tm: { url: 'tm-pr.oxylabs.io', port: 46000 },
    uz: { url: 'uz-pr.oxylabs.io', port: 47000 },
    ye: { url: 'ye-pr.oxylabs.io', port: 48000 },
    al: { url: 'al-pr.oxylabs.io', port: 49000 },
    ad: { url: 'ad-pr.oxylabs.io', port: 10000 },
    at: { url: 'at-pr.oxylabs.io', port: 11000 },
    ba: { url: 'ba-pr.oxylabs.io', port: 13000 },
    bg: { url: 'bg-pr.oxylabs.io', port: 14000 },
    by: { url: 'by-pr.oxylabs.io', port: 15000 },
    hr: { url: 'hr-pr.oxylabs.io', port: 16000 },
    cy: { url: 'cy-pr.oxylabs.io', port: 35000 },
    cz: { url: 'cz-pr.oxylabs.io', port: 18000 },
    dk: { url: 'dk-pr.oxylabs.io', port: 19000 },
    ee: { url: 'ee-pr.oxylabs.io', port: 20000 },
    fi: { url: 'fi-pr.oxylabs.io', port: 21000 },
    hu: { url: 'hu-pr.oxylabs.io', port: 23000 },
    is: { url: 'is-pr.oxylabs.io', port: 24000 },
    ie: { url: 'ie-pr.oxylabs.io', port: 25000 },
    lv: { url: 'lv-pr.oxylabs.io', port: 26000 },
    li: { url: 'li-pr.oxylabs.io', port: 27000 },
    lt: { url: 'lt-pr.oxylabs.io', port: 28000 },
    lu: { url: 'lu-pr.oxylabs.io', port: 29000 },
    mt: { url: 'mt-pr.oxylabs.io', port: 30000 },
    mc: { url: 'mc-pr.oxylabs.io', port: 31000 },
    md: { url: 'md-pr.oxylabs.io', port: 32000 },
    me: { url: 'me-pr.oxylabs.io', port: 33000 },
    no: { url: 'no-pr.oxylabs.io', port: 34000 },
    ro: { url: 'ro-pr.oxylabs.io', port: 35000 },
    rs: { url: 'rs-pr.oxylabs.io', port: 36000 },
    sk: { url: 'sk-pr.oxylabs.io', port: 37000 },
    si: { url: 'si-pr.oxylabs.io', port: 38000 },
    ch: { url: 'ch-pr.oxylabs.io', port: 39000 },
    mk: { url: 'mk-pr.oxylabs.io', port: 40000 },
    bs: { url: 'bs-pr.oxylabs.io', port: 41000 },
    bz: { url: 'bz-pr.oxylabs.io', port: 42000 },
    vg: { url: 'vg-pr.oxylabs.io', port: 43000 },
    cr: { url: 'cr-pr.oxylabs.io', port: 44000 },
    cu: { url: 'cu-pr.oxylabs.io', port: 45000 },
    dm: { url: 'dm-pr.oxylabs.io', port: 46000 },
    ht: { url: 'ht-pr.oxylabs.io', port: 47000 },
    hn: { url: 'hn-pr.oxylabs.io', port: 48000 },
    jm: { url: 'jm-pr.oxylabs.io', port: 49000 },
    aw: { url: 'aw-pr.oxylabs.io', port: 10000 },
    pa: { url: 'pa-pr.oxylabs.io', port: 11000 },
    pr: { url: 'pr-pr.oxylabs.io', port: 12000 },
    tt: { url: 'tt-pr.oxylabs.io', port: 13000 },
    fj: { url: 'fj-pr.oxylabs.io', port: 14000 },
    nz: { url: 'nz-pr.oxylabs.io', port: 15000 },
    bo: { url: 'bo-pr.oxylabs.io', port: 16000 },
    py: { url: 'py-pr.oxylabs.io', port: 17000 },
    uy: { url: 'uy-pr.oxylabs.io', port: 18000 },
    ve: { url: 've-pr.oxylabs.io', port: 19000 },
    pr: { url: 'pr.oxylabs.io', port: 7777 },
  };
  let proxy = entrypoints[country.toLowerCase()];
  if (proxy === undefined && proxy) {
    proxy = entrypoints['pr'];
  }
  return proxy;
}

function getBucketFileName(request) {
  let userId = uuid.v1();
  let yearMonth = formatDate(new Date(), 'yyyymm');
  let response = {};
  if (request.body.metadata.firstCapture) {
    response.imageFileName = `images/${request.body.clientPublicId}/firstCapture/${userId}-${request.body.country}-screen.png`;
    response.htmlFileName = `content/${request.body.clientPublicId}/firstCapture/${userId}-${request.body.country}-content.html`;
  } else {
    response.imageFileName = `images/${request.body.clientPublicId}/${yearMonth}/${request.body.metadata.monitor_name}/${request.body.metadata.traceUUID}/${userId}-${request.body.country}-screen.png`;
    response.htmlFileName = `content/${request.body.clientPublicId}/${yearMonth}/${request.body.metadata.monitor_name}/${request.body.metadata.traceUUID}/${userId}-${request.body.country}-content.html`;
  }
  return response;
}

function formatDate(date, format) {
  const map = {
    mm: date.getMonth() + 1,
    dd: date.getDate(),
    yy: date.getFullYear().toString().slice(-2),
    yyyy: date.getFullYear(),
    hh: date.getHours() < 10 ? `0${date.getHours()}` : date.getHours(),
    MM: date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes(),
    ss: date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds(),
  };

  return format.replace(/mm|dd|yy|yyyy|hh|MM|ss/gi, (matched) => map[matched]);
}

function responseMessageStatus(request, statusCode) {
  return {
    statusCode: statusCode,
    clientPublicId: request.body.clientPublicId,
    url: request.body.url,
    country: request.body.country,
    htmlPath: '',
    imagePath: '',
    metadata: request.body.metadata,
  };
}

async function responseMessageHtml(request, htmlData, statusCode, bucket) {
  let htmlFileName = getBucketFileName(request).htmlFileName;
  const htmlContent = htmlData === '' ? '' : await bucket.uploadBucket(htmlData, htmlFileName);
  return {
    statusCode: statusCode,
    clientPublicId: request.body.clientPublicId,
    url: request.body.url,
    country: request.body.country,
    htmlPath: htmlContent,
    imagePath: '',
    metadata: request.body.metadata,
  };
}

async function responseMessageImage(request, screenData, bucket) {
  let htmlFileName = getBucketFileName(request).htmlFileName;
  let imageFileName = getBucketFileName(request).imageFileName;

  const htmlContent = screenData.html === '' ? '' : await bucket.uploadBucket(screenData.html, htmlFileName);
  const imageContent = screenData.image === '' ? '' : await bucket.uploadBucket(screenData.image, imageFileName);

  return {
    statusCode: screenData.status,
    clientPublicId: request.body.clientPublicId,
    url: request.body.url,
    country: request.body.country,
    htmlPath: htmlContent,
    imagePath: imageContent,
    metadata: request.body.metadata,
  };
}

module.exports = {
  take: (req, config, bucket, broker) => {
    let proxy = getProxy(req.body.country);
    let message = {};
    switch (req.body.metadata.captureType.toLowerCase()) {
      case 'status':
        let status = new StatusService();
        status.configureProxy(proxy);
        status
          .takeStatus(req.body.url, config.userAgent)
          .then((data) => {
            message = responseMessageStatus(req, data);
            console.log(message);
            broker
              .push(message)
              .then(() => {
                console.log(`\u{1F64C} send kafka`);
              })
              .catch((error) => {
                console.error(error);
              });
          })
          .catch((error) => {
            console.log(error);
          });
        break;
      case 'html':
        let html = new HtmlService();
        html.configureProxy(proxy);
        html
          .takeHtml(req.body.url, config.userAgent)
          .then((data) => {
            responseMessageHtml(req, data.data, data.status, bucket)
              .then((dataBucket) => {
                broker
                  .push(dataBucket)
                  .then(() => {
                    console.log(`\u{1F64C} send kafka`);
                  })
                  .catch((error) => {
                    console.error(error);
                  });
              })
              .catch((error) => {
                console.log(error);
              });
          })
          .catch((error) => {
            console.log(error);
          });
        break;
      case 'image':
      default:
        let browser = new BrowserService();
        browser.configureProxy(proxy);
        browser
          .takeScreenShot(req.body.url)
          .then((data) => {
            responseMessageImage(req, data, bucket)
              .then((dataImage) => {
                console.log(dataImage);
                broker
                  .push(dataImage)
                  .then(() => {
                    console.log(`\u{1F64C} send kafka`);
                  })
                  .catch((error) => {
                    console.error(error);
                  });
              })
              .catch((error) => {
                console.log(error);
              });
          })
          .catch((error) => {
            console.log(error);
          });
        break;
    }
  },
  doesRequestHaveCaptureTypeAndFirstCapture: (req) => {
    if (req.body.metadata.captureType === undefined) req.body.metadata.captureType = 'image';
    if (req.body.metadata.firstCapture !== undefined && req.body.metadata.firstCapture) req.body.metadata.firstCapture = true;
  },
};
