const express = require('express');
const actions = require('../template/Actions');

class ExpressController {
  constructor(options) {
    this.environmentConfiguration = options.environmentConfiguration;
    this.bucketService = options.bucketService;
    this.brokerService = options.brokerService;
  }
  start() {
    this.app = express();
    this.app.use(express.json());
    this.actions();
    this.app.listen(7443, function () {
      console.log(`\u{1F6F8} Image Capture App listening at port 7443`);
    });
  }
  actions() {
    this.app.post('/webcapture/api/v1/screenshots', (req, res) => {});
    this.app.post('/api/v1/screenshots', (req, res) => {
      res
        .status(200)
        .send({ message: `Message received  client: ${req.body.clientPublicId}, url: ${req.body.url}, country: ${req.body.country}` });
      actions.doesRequestHaveCaptureTypeAndFirstCapture(req);
      actions.take(req, this.environmentConfiguration, this.bucketService, this.brokerService);
    });
    this.app.get('/health', (req, res) => {
      res.status(200).send({ ok: 'ok' });
    });
  }
}

module.exports = ExpressController;
